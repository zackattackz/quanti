package com.plustwoprojects.quanti;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QuantiApplication {

	public static void main(String[] args) {
		SpringApplication.run(QuantiApplication.class, args);
	}

}
