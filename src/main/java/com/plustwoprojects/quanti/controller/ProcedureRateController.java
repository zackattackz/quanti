package com.plustwoprojects.quanti.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import com.plustwoprojects.quanti.csv.CSVHelper;
import com.plustwoprojects.quanti.model.ProcedureRate;
import com.plustwoprojects.quanti.service.ProcedureRateService;

@RestController
@RequestMapping("/api/procedurerate")
public class ProcedureRateController {

	private final ProcedureRateService procedureRateService;

	Logger log = LoggerFactory.getLogger(ProcedureRateController.class);

	@Autowired
	public ProcedureRateController(ProcedureRateService procedureRateService) {
		this.procedureRateService = procedureRateService;
	}

	@GetMapping("/")
	public List<ProcedureRate> getAllProcedureRates() {
		try {
			System.out.println("get");
			return procedureRateService.getAllProcedureRates();
		} catch (Exception e) {
			log.error("getAllProcedureRates", e);
			throw new ResponseStatusException(
					HttpStatus.INTERNAL_SERVER_ERROR,
					e.toString());
		}
	}

	@PostMapping("/")
	public long addProcedureRate(@RequestBody ProcedureRate procedureRate){
		try {
			return procedureRateService.addProcedureRate(procedureRate);
		} catch (Exception e) {
			log.error("addProcedureRate", e);
			throw new ResponseStatusException(
					HttpStatus.INTERNAL_SERVER_ERROR,
					e.toString());
		}
	}

	@PostMapping("/importCSV")
	public void importCSV(@RequestParam("file") MultipartFile file) {
		try {
			procedureRateService.importCSV(file);
		} catch (Exception e) {
			log.error("importCSV", e);
			throw new ResponseStatusException(
					HttpStatus.INTERNAL_SERVER_ERROR,
					e.toString());
		}
	}
}
