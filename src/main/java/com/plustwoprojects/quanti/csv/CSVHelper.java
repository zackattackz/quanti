package com.plustwoprojects.quanti.csv;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.csv.QuoteMode;

import org.springframework.web.multipart.MultipartFile;

import com.plustwoprojects.quanti.model.ProcedureRate;

public class CSVHelper {
	public static String TYPE = "text/csv";

	public static boolean hasCSVFormat(MultipartFile file) {
		if (!TYPE.equals(file.getContentType())) {
			return false;
		}
		return true;
	}

	public static List<ProcedureRate> csvToProcedureRates(InputStream is) {
		try (BufferedReader fileReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
				CSVParser csvParser = new CSVParser(fileReader,
					CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());) {
			List<ProcedureRate> procedureRates = new ArrayList<ProcedureRate>();
			Iterable<CSVRecord> csvRecords = csvParser.getRecords();
			for (CSVRecord csvRecord : csvRecords) {
				ProcedureRate procedureRate = new ProcedureRate(
						null,
						Integer.parseInt(csvRecord.get("tq_id")),
						Integer.parseInt(csvRecord.get("medicare_provider_id")),
						csvRecord.get("provider"),
						csvRecord.get("health_system"),
						Integer.parseInt(csvRecord.get("cpt")),
						csvRecord.get("description"),
						Integer.parseInt(csvRecord.get("rev_code")),
						Double.parseDouble(csvRecord.get("rate")),
						csvRecord.get("raw_plan"),
						csvRecord.get("mapped_payer"),
						csvRecord.get("payer_class"));
				procedureRates.add(procedureRate);
			}
			return procedureRates;
		} catch (IOException e) {
			throw new RuntimeException("fail to parse CSV file: " + e.getMessage());
		}
	}

	public static ByteArrayInputStream procedureRatesToCSV(List<ProcedureRate> procedureRates) {
		final CSVFormat format = CSVFormat.DEFAULT.withQuoteMode(QuoteMode.MINIMAL);

		try (ByteArrayOutputStream out = new ByteArrayOutputStream();
				CSVPrinter csvPrinter = new CSVPrinter(new PrintWriter(out), format);) {
			for (ProcedureRate procedureRate : procedureRates) {
				List<String> data = Arrays.asList(
						String.valueOf(procedureRate.getId()),
						String.valueOf(procedureRate.getTqId()),
						String.valueOf(procedureRate.getMedicareProviderId()),
						procedureRate.getProvider(),
						procedureRate.getHealthSystem(),
						String.valueOf(procedureRate.getCpt()),
						procedureRate.getDescription(),
						String.valueOf(procedureRate.getRevCode()),
						String.valueOf(procedureRate.getRate()),
						procedureRate.getMappedPayer(),
						procedureRate.getPayerClass());

				csvPrinter.printRecord(data);
			}

			csvPrinter.flush();
			return new ByteArrayInputStream(out.toByteArray());
		} catch (IOException e) {
			throw new RuntimeException("fail to import data to CSV file: " + e.getMessage());
		}
	}
}

