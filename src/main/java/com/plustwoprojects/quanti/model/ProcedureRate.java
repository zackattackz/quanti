package com.plustwoprojects.quanti.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "procedure_rate")
public class ProcedureRate {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	@Column(name = "tq_id")
	private int tqId;
	@Column(name = "medicare_provider_id")
	private int medicareProviderId;
	@Column(name = "provider")
	private String provider;
	@Column(name = "health_system")
	private String healthSystem;
	@Column(name = "cpt")
	private int cpt;
	@Column(name = "description")
	private String description;
	@Column(name = "rev_code")
	private int revCode;
	@Column(name = "rate")
	private double rate;
	@Column(name = "raw_plan")
	private String rawPlan;
	@Column(name = "mapped_payer")
	private String mappedPayer;
	@Column(name = "payer_class")
	private String payerClass;

	protected ProcedureRate(){
	}
	public ProcedureRate(
			Long id,
			int tqId,
			int medicareProviderId,
			String provider,
			String healthSystem,
			int cpt,
			String description,
			int revCode,
			double rate,
			String rawPlan,
			String mappedPayer,
			String payerClass) {
		this.id = id;
		this.tqId = tqId;
		this.medicareProviderId = medicareProviderId;
		this.provider = provider;
		this.healthSystem = healthSystem;
		this.cpt = cpt;
		this.description = description;
		this.revCode = revCode;
		this.rate = rate;
		this.rawPlan = rawPlan;
		this.mappedPayer = mappedPayer;
		this.payerClass = payerClass;
			}

	/**
	 * Get id.
	 *
	 * @return id as Long.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Set id.
	 *
	 * @param id the value to set.
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Get tqId.
	 *
	 * @return tqId as int.
	 */
	public int getTqId() {
		return tqId;
	}

	/**
	 * Set tqId.
	 *
	 * @param tqId the value to set.
	 */
	public void setTqId(int tqId) {
		this.tqId = tqId;
	}

	/**
	 * Get medicareProviderId.
	 *
	 * @return medicareProviderId as int.
	 */
	public int getMedicareProviderId() {
		return medicareProviderId;
	}

	/**
	 * Set medicareProviderId.
	 *
	 * @param medicareProviderId the value to set.
	 */
	public void setMedicareProviderId(int medicareProviderId) {
		this.medicareProviderId = medicareProviderId;
	}

	/**
	 * Get provider.
	 *
	 * @return provider as String.
	 */
	public String getProvider() {
		return provider;
	}

	/**
	 * Set provider.
	 *
	 * @param provider the value to set.
	 */
	public void setProvider(String provider) {
		this.provider = provider;
	}

	/**
	 * Get healthSystem.
	 *
	 * @return healthSystem as String.
	 */
	public String getHealthSystem() {
		return healthSystem;
	}

	/**
	 * Set healthSystem.
	 *
	 * @param healthSystem the value to set.
	 */
	public void setHealthSystem(String healthSystem) {
		this.healthSystem = healthSystem;
	}

	/**
	 * Get cpt.
	 *
	 * @return cpt as int.
	 */
	public int getCpt() {
		return cpt;
	}

	/**
	 * Set cpt.
	 *
	 * @param cpt the value to set.
	 */
	public void setCpt(int cpt) {
		this.cpt = cpt;
	}

	/**
	 * Get description.
	 *
	 * @return description as String.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Set description.
	 *
	 * @param description the value to set.
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Get revCode.
	 *
	 * @return revCode as int.
	 */
	public int getRevCode() {
		return revCode;
	}

	/**
	 * Set revCode.
	 *
	 * @param revCode the value to set.
	 */
	public void setRevCode(int revCode) {
		this.revCode = revCode;
	}

	/**
	 * Get rate.
	 *
	 * @return rate as double.
	 */
	public double getRate() {
		return rate;
	}

	/**
	 * Set rate.
	 *
	 * @param rate the value to set.
	 */
	public void setRate(double rate) {
		this.rate = rate;
	}

	/**
	 * Get rawPlan.
	 *
	 * @return rawPlan as String.
	 */
	public String getRawPlan() {
		return rawPlan;
	}

	/**
	 * Set rawPlan.
	 *
	 * @param rawPlan the value to set.
	 */
	public void setRawPlan(String rawPlan) {
		this.rawPlan = rawPlan;
	}

	/**
	 * Get mappedPayer.
	 *
	 * @return mappedPayer as String.
	 */
	public String getMappedPayer() {
		return mappedPayer;
	}

	/**
	 * Set mappedPayer.
	 *
	 * @param mappedPayer the value to set.
	 */
	public void setMappedPayer(String mappedPayer) {
		this.mappedPayer = mappedPayer;
	}

	/**
	 * Get payerClass.
	 *
	 * @return payerClass as String.
	 */
	public String getPayerClass() {
		return payerClass;
	}

	/**
	 * Set payerClass.
	 *
	 * @param payerClass the value to set.
	 */
	public void setPayerClass(String payerClass) {
		this.payerClass = payerClass;
	}
}
