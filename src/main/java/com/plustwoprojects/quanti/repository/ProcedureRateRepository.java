package com.plustwoprojects.quanti.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.plustwoprojects.quanti.model.ProcedureRate;

public interface ProcedureRateRepository extends JpaRepository<ProcedureRate, Long> {
}
