package com.plustwoprojects.quanti.service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.plustwoprojects.quanti.model.ProcedureRate;
import com.plustwoprojects.quanti.repository.ProcedureRateRepository;

import com.plustwoprojects.quanti.csv.CSVHelper;

@Service
public class DefaultProcedureRateService implements ProcedureRateService {

	private final ProcedureRateRepository procedureRateRepository;

	@Autowired
	public DefaultProcedureRateService(ProcedureRateRepository procedureRateRepository) {
		this.procedureRateRepository = procedureRateRepository;
	}

	@Override
	public List<ProcedureRate> getAllProcedureRates() {
		return procedureRateRepository.findAll();
	}

	@Override
	public long addProcedureRate(ProcedureRate procedureRate) {
		return procedureRateRepository.save(procedureRate).getId();
	}

	@Override
	public void deleteProcedureRate(long id) {
		procedureRateRepository.deleteById(id);
	}

	@Override
	public List<Long> importCSV(MultipartFile file) {
		List<Long> ids = new ArrayList<Long>();
		try {
			List<ProcedureRate> procedureRates = CSVHelper.csvToProcedureRates(file.getInputStream());
			for(ProcedureRate procedureRate : procedureRateRepository.saveAll(procedureRates)) {
				ids.add(new Long(procedureRate.getId()));
			}
			return ids;
		} catch (IOException e) {
			throw new RuntimeException("fail to store csv data: " + e.getMessage());
		}
	}

	@Override
	public ByteArrayInputStream exportCSV() {
		List<ProcedureRate> procedureRates = procedureRateRepository.findAll();

		ByteArrayInputStream bytes = CSVHelper.procedureRatesToCSV(procedureRates);
		return bytes;
	}
}
