package com.plustwoprojects.quanti.service;

import java.io.ByteArrayInputStream;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.plustwoprojects.quanti.model.ProcedureRate;

public interface ProcedureRateService {

	public List<ProcedureRate> getAllProcedureRates();

	public long addProcedureRate(ProcedureRate procedureRate);

	public void deleteProcedureRate(long id);

	public List<Long> importCSV(MultipartFile file);

	public ByteArrayInputStream exportCSV();
}
